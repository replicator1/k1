import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      Color [] balls = new ColorSort.Color [10];

      for (int i=0; i < balls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            balls[i] = ColorSort.Color.red;
         } else  if (rnd > 2./3.) {
            balls[i] = ColorSort.Color.blue;
         } else {
            balls[i] = ColorSort.Color.green;
         }
      }
      System.out.println(Arrays.toString(balls));
      ColorSort.reorder (balls);
      System.out.println(Arrays.toString(balls));


      // [blue, green, green, green, red, red, red, green, red, green]
      // [red, blue, red, blue, red, green, red, green, blue, red]
      Color[] balls2 = new Color[]{Color.blue, Color.green, Color.red, Color.green, Color.red, Color.green};
      reorder(balls2);
      System.out.println(Arrays.toString(balls2));
   }

   public static void reorder (Color[] balls) {
      int indexRed = 0;
      int indexBlue = balls.length -1;

      for (int i = 0; i <= indexBlue; i++) {

         if (balls[i].equals(Color.red)) {
            balls[i] = balls[indexRed];
            balls[indexRed] = Color.red;
            indexRed++;

         } else if (balls[i].equals(Color.blue)) {
            balls[i] = balls[indexBlue];
            balls[indexBlue] = Color.blue;
            indexBlue--;
            i--;
         }
      }
   }
}
